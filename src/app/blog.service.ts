
import { Injectable } from '@angular/core';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  constructor(public router: Router) {
   }

   public goToStory(story: number) {
    this.router.navigate(['page/' + story]);
   }
}

import { StoryAction } from './StoryAction';

export interface Choice {
    id: number;
    description: string;
    action: StoryAction[];
}

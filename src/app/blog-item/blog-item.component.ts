import { Choice } from './../Choice';
import { BlogService } from './../blog.service';
import { Component, OnInit } from '@angular/core';
import allStory from '../../assets/blog.json';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-blog-item',
  templateUrl: './blog-item.component.html',
  styleUrls: ['./blog-item.component.scss']
})
export class BlogItemComponent implements OnInit {

  // tslint:disable-next-line: max-line-length
  choice = allStory;
  story: Choice;
  constructor(private blogService: BlogService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.getStory(+params.id);
    });
  }
  public getStory(idStory: number) {
    console.log(this.choice, idStory);
    this.story = this.choice.find(x => x.id === idStory);
    console.log(this.story);
  }

  public goToStory(storyId: number) {
    this.blogService.goToStory(storyId);
  }
}

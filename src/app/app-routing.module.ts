import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogItemComponent } from './blog-item/blog-item.component';

const routes: Routes = [
  { path: 'page/:id', component: BlogItemComponent },
  { path: '', redirectTo: 'page/1', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
      { enableTracing: true }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
